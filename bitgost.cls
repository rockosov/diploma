\DeclareOption*{\PassOptionsToClass{\CurrentOption}{disser}}
\ProcessOptions\relax
\LoadClass[specialist, a4paper, 12pt, times]{disser}

% -----------------------------------------------------------------------------------------------------------------------------------
% Величины стиля
% Размер шрифта
\newlength{\bitgostfontsize}
\setlength{\bitgostfontsize}{12pt}

% Межстрочный интервал
\newlength{\bitgostspacingize}
\setlength{\bitgostspacingize}{1.5\bitgostfontsize}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Пакеты
\usepackage[        %
	a4paper,        % тип листа А4
	left=3cm,       % левый отступ
	right=1cm,      % правый отступ
	top=2cm,        % верхний отступ
	bottom=2cm,     % нижний отступ
	nohead,         % нет верхнего колонтитула
	footskip=0.75cm % от текста до нижнего колонтитула 12.5мм
]{geometry}

% Описание класса документа
% specialist - тип документа
% href - для добавления ссылок
% notitlepage - пока не используем титульник
% 14pt - размер шрифта документа
% times - шрифт Times как основной

% Кодировки
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english,russian]{babel}

% Для оформления рисунков
\usepackage{graphicx}

% Для оформления листингов
\usepackage{listings}

% Для оформления блок-схем
\usepackage{tikz}

% Для плавающих объектов
\usepackage{float}

% Для оформления псевдокода
\usepackage[lined,vlined,boxed,commentsnumbered,algochapter,linesnumbered]{algorithm2e}

% Для добавление команд в окружение
\let\deflength\@undefined
\usepackage{etoolbox}

% Для настройки подписей
\usepackage{caption}

% Для удобного добавления рисунков в формате eps
\ifpdf\usepackage{epstopdf}\fi

% Для оформления URL
\usepackage{url}

% Для увеличения количества float элементов без ссылок (полезно, когда указываем отрезок из нескольких float'ов)
\usepackage{morefloats}

% Для flush-операции \FloatBarrier, которая немедленно записывает все float-объекты выше нее
\usepackage{placeins}

% Для таблиц, выравненных по ширине
\usepackage{tabularx}

% Для настройки счетчиков
\usepackage{totalcount}

% Для настройки списков
\usepackage{enumitem}

\usepackage{longtable}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройка глав, которые не нумеруются
\def\@snchapter#1{
	\if@twocolumn
		\@topnewpage[\@makesnchapterhead{#1}]%
	\else
		\@makesnchapterhead{#1}%
		\@afterheading
	\fi
}

\def\@makesnchapterhead#1{{%
	\parindent\z@
	\interlinepenalty\@M
	\centering\chapterfont\beforechapter #1\afterchapter
}}

% Глава попадает в содержание и не нумеруется
\renewcommand\nchapter[1]{%
	\if@openright
		\cleardoublepage
	\else
		\clearpage
	\fi
	\thispagestyle{\@chapterpagestyle}%
	\global\@topnum\z@
	\hrefphantom{chapter}
	\addcontentsline{toc}{chapter}{#1}%
	\@afterindentfalse
	\@snchapter{#1}
}

% Глава не попадает в содержание и не нумеруется
\newcommand\ntocchapter[1]{%
	\thispagestyle{\@chapterpagestyle}%
	\global\@topnum\z@
	\hrefphantom{chapter}
	\@afterindentfalse
	\@snchapter{#1}
}

% Для того, чтобы содержание подчинялось стилю данных глав
\renewcommand\tocsection{\ntocchapter{\contentsname}}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки страницы

% Номера страниц снизу и по центру
\pagestyle{footcenter}
\chapterpagestyle{footcenter}

% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки оглавления

% Включать подсекции в оглавление
\setcounter{tocdepth}{3}

\renewcommand{\tocprethechapter}{} % перед номером главы в содержании нет ничего
\renewcommand{\tocpostthechapter}{~} % после номера главы в содержании идет пробел
\renewcommand{\tocchapterfont}{\normalsize} % шрифт главы в содержании
% Переопределение записи о главе в оглавлении для удаления вертикального отступа между записями
\renewcommand*\l@chapter[2]{%
	\ifnum \c@tocdepth >\m@ne
		\addpenalty{-\@highpenalty}
		\setlength\@tempdima{1.5em}
		\begingroup
			\parindent \z@ \rightskip \@pnumwidth
			\parfillskip -\@pnumwidth
			\leavevmode\tocchapterfont
			\advance\leftskip\@tempdima
			\hskip -\leftskip
			#1\nobreak
			\tocchapterfillfont\tocchapterfill\hfill
			\nobreak\hb@xt@\@pnumwidth{\hss\tocchapternumfont #2}\par
			\penalty\@highpenalty
		\endgroup
	\fi
}

\renewcommand{\tocprethesection}{} % перед номером секции в содержании нет ничего
\renewcommand{\tocsectionnameindent}{2em} % сами регулируем заполнение пространства между номером секции и именем секции
                                          % XXX: может быть проблема при секциях, которые нумеруются уже двузначными числами,
                                          % или принадлежат главам с двузначными номерами),
                                          % тогда стоит поиграться с этим параметром
\renewcommand{\tocpostthesection}{~} % после номера секции в содержании идет один пробел
\renewcommand{\tocsectionindent}{0cm} % отступ для названия секции в содержании

\renewcommand{\tocprethesubsection}{} % перед номером подсекции в содержании нет ничего
\renewcommand{\tocsubsectionnameindent}{3em} % сами регулируем заполнение пространства между номером подсекции и именем подсекции
\renewcommand{\tocpostthesubsection}{~} % после номера подсекции в содержании идет один пробел
\renewcommand{\tocsubsectionindent}{0cm} % отступ для названия подсекции в содержании
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки для главы
\renewcommand{\thechapterfont}{\normalsize\bfseries} % нормальный размер у номера главы, полужирный шрифт
\renewcommand{\chapterfont}{\normalsize\bfseries} % нормальный размер у имени главы, полужирный шрифт
\renewcommand{\postthechapter}{~} % после номера главы идет только пробел
\renewcommand{\prethechapter}{} % перед номером главы нет ничего
\renewcommand{\chapteralign}{\raggedright} % имя главы выравнивается по левому краю
\renewcommand{\chapterindent}{1.25cm} % отступ для названия главы
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки секций
\renewcommand{\sectionfont}{\normalsize\bfseries} % нормальный размер у имени секции, полужирный шрифт
\renewcommand{\sectionindent}{1.25cm} % отступ для названия секции
\renewcommand{\prethesection}{} % перед номером секции нет ничего
\renewcommand{\postthesection}{~} % после номера секции идет только один пробел
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки подсекций
\renewcommand{\subsectionfont}{\normalsize\bfseries} % нормальный размер у имени подсекции, полужирный шрифт
\renewcommand{\subsectionindent}{1.25cm} % отступ для названия подсекции
\renewcommand{\prethesubsection}{} % перед номером подсекции нет ничего
\renewcommand{\postthesubsection}{~} % после номера подсекции идет только один пробел
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки подподсекций
\renewcommand{\subsubsectionfont}{\normalsize\bfseries} % нормальный размер у имени подподсекции, полужирный шрифт
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки приложений
\renewcommand{\appendixfont}{\normalsize\bfseries} % нормальный размер у имени приложения, полужирный шрифт
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки текста
\setlength\parindent{1.25cm} % абзацный отступ

% Выравнивание текста по ширине без переносов с разреженными строками
% \tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000

% Точка с запятой в качестве разделителя между номерами цитирований
%\setcitestyle{semicolon}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки подписей и меток
\renewcommand{\captionlabeldelim}{ --} % разделитель метки и подписи
\renewcommand{\captionfont}{\normalsize} % шрифт подписи
\renewcommand{\captionlabelfont}{\normalsize} % шрифт метки
% Настройка всех подписей: расположение посередине, разделитель - тире
\captionsetup{format=plain,justification=centering,labelsep=endash}
% Для таблиц подпись равняем по левому краю
\captionsetup[longtable]{justification=raggedright,singlelinecheck=false}
\captionsetup[table]{justification=raggedright,singlelinecheck=false}
\setlength{\abovecaptionskip}{0pt} % расстояние между объектом и подписью
\setlength{\belowcaptionskip}{0pt} % расстояние после подписи
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки таблиц, листингов

% Определение формата листинга
\lstset{
	basicstyle=\ttfamily\small, % моноширинный шрифт и размер small
	breaklines=true,            % автоматические переносы длинных строк
	tabsize=4,                  % табуляция в 4 пробела
	columns=fullflexible,       % настройка колонок для полной гибкости
	lineskip={-1.5pt},          % межстрочный интервал равен 1
	showstringspaces=false,     % запрещаем показывать пробелы
	aboveskip=8pt,              % отступ до листинга -- один межстрочный интервал
	                            % (учитывая, что в листинге один межстрочный интервал равен 10pt, то прибавим еще 8pt)
}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки рисунков
\graphicspath{{figures/}} % рисунки в этой папке
\AtBeginEnvironment{figure}{\vskip \bitgostspacingize} % один межстрочный интервал до фигуры
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки маркированных перечислений
\renewcommand{\labelitemi}{$-$} % первый уровень вложенности дефисами
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки нумерованных перечислений

% Другой алфавит по ГОСТу
\def\asbukbiggost#1{\expandafter\russian@alph\csname c@#1\endcsname}
\def\russian@alph#1{\ifcase#1\or
	а\or б\or в\or г\or д\or е\or ж\or
	и\or к\or л\or м\or н\or
	п\or р\or с\or т\or у\or ф\or х\or
	ц\or ш\or щ\or э\or ю\or я\else\xpg@ill@value{#1}{russian@alph}\fi}

% Метки первого и второго уровня вложенности
\renewcommand\theenumi {\asbukbiggost{enumi}}
\renewcommand\theenumii {\arabic{enumii}}
\renewcommand\labelenumi {\theenumi)}
\renewcommand\labelenumii {\theenumii)}

% Настройки геометрии перечислений
\newlength{\enumlabelindenti}
\newlength{\enumlabelindentii}
\setlength{\enumlabelindenti}{\parindent}
\addtolength{\enumlabelindenti}{1ex}
\setlength{\enumlabelindentii}{2\parindent}

\AddEnumerateCounter{\asbukbiggost}{\@asbukbiggost}{м)}
\setlist[enumerate,1]{nolistsep, itemsep=0pt, topsep=0pt, leftmargin=0cm, labelindent=\enumlabelindenti, itemindent=*}
\setlist[enumerate,2]{nolistsep, itemsep=0pt, topsep=0pt, leftmargin=\parindent, labelindent=\enumlabelindentii, labelsep=*, itemindent=*}
\setlist[itemize,1]{nolistsep, itemsep=0pt, topsep=0pt, leftmargin=0cm, labelindent=\parindent, labelsep=*, itemindent=*}
\setlist[itemize,2]{nolistsep, itemsep=0pt, topsep=0pt, leftmargin=\parindent, labelindent=2\parindent, labelsep=*, itemindent=*}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки библиографического списка
\bibliographystyle{gost705} % для его оформления используем ГОСТ.7.05
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки отступов
% Отступ между заголовками глав, секций, подсекций - один межстрочный интервал
\newlength{\skipbetweentitles}
\setlength{\skipbetweentitles}{\bitgostspacingize}
% Отступ перед текстом - один межстрочный интервал
\newlength{\beforetext}
\setlength{\beforetext}{\bitgostspacingize}
\addtolength{\beforetext}{-\skipbetweentitles}
% Отступ после текста - два межстрочных интервала
\newlength{\aftertext}
\setlength{\aftertext}{2\bitgostspacingize}
% Команды для установки начала и конца текста текущей структурной единицы документа (главы, секции или подсекции)
\newcommand{\starttext}{\vskip \beforetext}
\newcommand{\stoptext}{\vskip \aftertext}
\newcommand{\stoptextwithfigure}{\vskip \bitgostspacingize}
% Отступ перед именем секции
\renewcommand{\beforesection}{\skipbetweentitles}
% Отступ после имени секции
\renewcommand{\aftersection}{\skipbetweentitles}
% Отступ перед именем подсекции
\renewcommand{\beforesubsection}{\skipbetweentitles}
% Отступ после имени подсекции
\renewcommand{\aftersubsection}{\skipbetweentitles}
% Отступ после имени главы
\renewcommand{\afterchapter}{\vskip \skipbetweentitles}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Переопределение стандартных заголовков
\AtBeginDocument{%
	\def\contentsname{СОДЕРЖАНИЕ}
	\def\figurename{Рисунок}
	\def\bibname{СПИСОК ИСПОЛЬЗОВАННЫХ ИСТОЧНИКОВ}
	\def\appendixname{ПРИЛОЖЕНИЕ}
	\def\introname{ВВЕДЕНИЕ}
	\def\conclusionname{ЗАКЛЮЧЕНИЕ}
}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройка псевдокода
% Переопределение стандартных имен
\SetKwInput{KwData}{Исходные параметры}
\SetKwInput{KwResult}{Результат}
\SetKwInput{KwIn}{Входные данные}
\SetKwInput{KwOut}{Выходные данные}
% Моноширинный шрифт везде
\SetAlFnt{\ttfamily\small}
\SetAlTitleSty{\ttfamily}
% Служебные слова выделяются курсивом
\newcommand{\newkwstyle}[1]{\textit{\ttfamily{#1}}}
\SetKwSty{newkwstyle}
\SetArgSty{texttt}
% Шрифт номер тоже моноширинный
\renewcommand{\NlSty}[1]{\textnormal{\texttt{#1}}}
% Разделитель между именем подписи и содержанием подписи
\SetAlgoCaptionSeparator{~--}
% Подпись не имеет дополнительных свойств по шрифту
\SetAlCapSty{}
% Псевдокод, как и листинг, маленького размера, около 10pt, и с одинарным отступом
\AtBeginEnvironment{algorithm}{\small\singlespacing}
% Отступ для номеров строк
\SetNlSkip{0.8em}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки URL
\urlstyle{same}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройки счетчиков
\DeclareTotalCounter{figure} % общее количество фигур
\DeclareTotalCounter{table} % общее количество таблиц
\DeclareTotalCounter{page} % общее количество страниц
% Общее количество используемой литературы
\newcounter{bibitem}
\DeclareTotalCounter{bibitem}
\let\orgbibitem\bibitem
\renewcommand\bibitem{\stepcounter{bibitem}\orgbibitem}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Настройка переключения в режим вставки приложений
\providecommand\tocpretheappendixchapter{\appendixname~}
\providecommand\posttheappendixskip{\par\nobreak\vskip \skipbetweentitles}
\newcommand\appendixstart{
	\appendix
	\renewcommand\thechapterfont{\normalsize\bfseries}
	\renewcommand\chapterfont{\normalsize\bfseries}
	\let\afterchapter=\oldafterchapter
	\let\postthechapter=\posttheappendixskip
	\let\tocprethechapter=\tocpretheappendixchapter % в содержании приложение помещается с тем же типом шрифта и именем
}
% -----------------------------------------------------------------------------------------------------------------------------------

% -----------------------------------------------------------------------------------------------------------------------------------
% Команда для настройки списка сокращений и обозначений
\newenvironment{abbreviation}{
	\list{}{
		\labelwidth \parindent
		\labelindent 2\parindent
		\itemindent 2\parindent
		\leftmargin 0cm
		\itemsep 0cm
		\let\makelabel\abbreviationlabel
	}
}{
	\endlist
}
\newcommand*\abbreviationlabel[1]{\hspace\labelsep\normalfont #1~--}
% -----------------------------------------------------------------------------------------------------------------------------------
