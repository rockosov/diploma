\section{Описание загрузки ядра XNU}
\label{sec:xnu_boot_process}

\subsection{Загрузчик boot.efi}
\starttext
\textbf{Загрузчик boot.efi} поставляется компанией Apple в сборке с ОС Mac OS X и не имеет открытого исходного кода. При исследовании работы данного загрузчика применялись техники reverse engineering. Boot.efi расположен в директории \textit{/System/Library/CoreServices} загрузочного раздела операционной системы. В отличие от других загрузчиков, boot.efi имеет FAT"=формат исполняемого файла, что позволяет включать в файл несколько исполняемых файлов разных архитектур. Далее рассматривается только архитектура X86\_64, хотя ее отличия от архитектуры X86 несущественны.
Рассмотрим функциональность загрузчика. Загрузчик экспортирует одну функцию –- \texttt{start(IN EFI\_HANDLE ImageHandle, IN EFI\_SYSTEM\_TABLE* SystemTable)}, в тело которой включена вся логика загрузчика. Диаграмма исполнения данной функции представлена на рисунке \ref{fig:boot_efi_flowchart}.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=0.7]{boot_efi_flowchart.png}}
	\caption{Диаграмма исполнения функции start() загрузчика boot.efi}
	\label{fig:boot_efi_flowchart}
\end{figure}

Основные этапы работы загрузчика boot.efi:
\begin{itemize}
	\item \textbf{InitEFIEnvironment} –- инициализация среды EFI, установка глобальных переменных, таких, как Boot"=сервисы, Runtime"=сервисы, системная таблица, Image Handle;
	\item \textbf{InitConsole} –- инициализация консоли;
	\item \textbf{InitDeviceTree} –- инициализация дерева устройств, которое в дальнейшем будет использовано операционной системой (рисунок \ref{fig:ioreg_device_tree});
	\item \textbf{выделение памяти для шлюза ядра} –- кода, который передаст управление от загрузчика ядру;
	\item \textbf{InitMemoryConfig, InitSupportedCpuTypes} –- дополнительная инициализация настроек загрузки;
	\item \textbf{CheckHibernate} –- проверка, была ли система в гибернации, если да –- загрузка системы из гибернации;
	\item \textbf{ProcessOptions} –- функция, получающая все опции загрузки, в том числе нажатые горячие клавиши из документации \cite{apple_ht1533};
	\item \textbf{Recovery Boot} –- проверка загрузки с recovery"=загрузчика, старт recovery"=загрузчика, если нужно;
	\item \textbf{SetConsoleMode} –- инициализация режима консоли;
	\item \textbf{DrawBootGraphics} –- отрисовка графики загрузчика;
	\item \textbf{LoadKernelCache} –- загрузка ядерного кэша (Mach-O файла, в котором слинкованы ядро и все расширения ядра, которые должны будут автоматически запуститься после старта системы) в память;
	\item \textbf{LoadKernel} –- если кэш ядра недоступен, то в память загружается файл ядра;
	\item \textbf{InitBootStruct} –- инциализация структуры boot"=аргументов, которая далее будет передана точке входа ядра в качестве параметра;
	\item \textbf{LoadDrivers} –- загрузка драйверов, которые находятся в \textit{/System/Library/Extensions.mkext} (Mach-O файле, в котором все расширения ядра слинкованы между собой, но не слинкованы с ядром);
	\item \textbf{LoadRamDisk} –- загрузка ядра с RAM"=раздела;
	\item \textbf{StopAnimation} –- остановка анимации загрузчика;
	\item \textbf{FinalizeBootStruct} –- структура boot"=аргументов готовится к передаче ядру, дополняется некоторыми параметрами (video"=аргументами), вызывается функция \texttt{ExitBootServices()}, которая сигнализирует среде EFI об остановке всех Boot"=сервисов, загрузке ядра ОС;
	\item \textbf{вызов шлюза ядра} –- подготовка процессора к вызову точки входа ядра, вызов точки входа ядра –- \texttt{pstart()}.
\end{itemize}

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=\textwidth]{ioreg_device_tree.png}}
	\caption{Дерево устройств IOKit}
	\label{fig:ioreg_device_tree}
\end{figure}
\stoptextwithfigure

\subsection{Основной алгоритм загрузки XNU}
\starttext
Ядро XNU является объектным файлом формата Mach-O. EFI"=загрузчик содержит код для разбора Mach-O файла, соответственно, умеет получать точку входа ядра из load"=команды с идентификатором \texttt{LC\_UNIXTHREAD}, которая содержит состояние регистров главного потока файла и адрес функции главного потока (состояние регистра rip). В Mountain Lion данная команда заменена на \texttt{LC\_MAIN}. На рисунке \ref{fig:xnu_boot_process_algorithm} изображен основной алгоритм загрузки ядра XNU \cite{mac_os_x_and_ios_internals_to_the_apples_core}.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=1]{xnu_boot_process_algorithm.png}}
	\caption{Основной алгоритм загрузки ядра XNU}
	\label{fig:xnu_boot_process_algorithm}
\end{figure}
\stoptextwithfigure

\subsection{Функция vstart}
\starttext
Данная функция является функцией инициализации ядра. Эта функция особенна тем, что выполняется как на master"=CPU, так и на slave"=CPU. Выполнение на slave"=CPU отличается передачей нулевого указателя на структуру \texttt{boot\_args\_start}, передающуюся в \texttt{vstart} в качестве параметра.
Последовательность действий функции \texttt{vstart}:
\begin{itemize}
	\item при загрузке на master"=CPU: \textbf{инициализирует отладку по COM-порту} с помощью функции \texttt{pal\_serial\_init()}, если ядро собрано с флагом DBG;
	\item \textbf{включает бит NX/XD}: на платформах x86\_64 NX (No Execute) предназначен для борьбы с инъекцией кода, если страницы памяти отмечены как data (часто это стек и куча), то обращение к ним (через rip) вызовет ошибку page fault, NX/XD бит будет установлен на всех процессорах, если они будут поддерживать данную функцию (CPUID\_EXTFEATURE\_XD);
	\item \texttt{cpu\_desc\_init[64]} -- данная функция \textbf{инициализирует GDT и LDT на master"=CPU}, функция сопровождается вызовом \texttt{cpu\_desc\_load(64)}, которая \textbf{загружает LDT ядра} для использования ее как на master"=CPU, так и на slave"=CPU;
	\item \texttt{cpu\_mode\_init} -- функция \textbf{инициализирует моделезависимые регистры процессора}, используемые для вызова системных вызовов через SYSENTER/SYSCALL), и физическое отображение страниц памяти (pmap);
	\item \texttt{i386\_init/i386\_init\_slave}: \textbf{вызываются master"=CPU и slave"=CPU}, соответственно.
\end{itemize}
\stoptext

\subsection{Функция i386\_init}
\starttext
Функция отвечает за инициализацию платформы. Инициализирует master"=CPU для использования и готовит его к загрузке ядра. \texttt{i386\_init\_slave} -- делает то же самое для slave"=CPU. Данная функция никогда не дойдет до завершения, так в ней вызываются функции старта системы.
Последовательность вызовов инициализации подсистем:
\begin{itemize}
	\item \texttt{pal\_i386\_init} -- \textbf{инициализация PAL (Platform Abstraction Layer)} -- по сути, инициализация блокировки EFI;
	\item \texttt{PE\_init\_Platform} -- \textbf{инициализация глобальной структуры PE\_state}, которая содержит копии boot"=аргументов, video"=аргументов и многое другое, эта функция вызывает \texttt{pe\_identify\_platform}, которая инициализирует глобальную переменную \texttt{gPEClockFrequencyInfo}, содержащую информацию о частоте работы CPU, шины и т.д.;
	\item \texttt{kernel\_early\_bootstrap} -- \textbf{инициализирует подсистему спинблокировок} (\texttt{lck\_mod\_init}) и \textbf{таймер} (\texttt{timer\_call\_initialize});
	\item \texttt{cpu\_init} -- \textbf{устанавливает конечный срок таймера для текущего CPU} в зловещее число \texttt{EndOfAllTime(0xFFFFFFFFFFFFFFFFULL)}, после этого вызывается \texttt{i386\_activate\_cpu}, которая стартует текущее CPU в логическом смысле;
	\item \texttt{printf\_init} -- \textbf{инициализация подсистемы вывода отладочных сообщений} (в случае, если подключен отладчик, возможен вывод сообщений в отладчик);
	\item \texttt{panic\_init} -- \texttt{инициализация подсистемы вывода panic"=сообщений} (в случае, если подключен отладчик, возможен вывод panic"=сообщений в отладчик);
	\item \texttt{PE\_init\_kprintf} -- \textbf{настойка вывода в один из подключенных отладчиков} (если есть);
	\item \textbf{проверка наличия serial"=консоли} -- проверяет serial boot"=аргумент, если он установлен, то вызывает \texttt{switch\_to\_serial\_console} для того, чтобы настроить serial"=консоль;
	\item \texttt{PE\_init\_printf} -- \textbf{инициализация функции printf в выбранную для отладки консоль};
	\item \textbf{проверка 64-битного процессора} -- проверяет бит CPUID\_EXTFEATURE\_EM64T, тем самым определяя 64-битный процессор, и, если ядро не запущено с аргументом legacy, определяет, что ядро запущено под 64-битным процессором, проверяет бит CPUID\_EXTFEATURE\_XD;
	\item \texttt{i386\_vm\_init} -- \textbf{переход управления виртуальной памятью от EFI к ядру}, вызов \texttt{pmap\_bootstrap} (инициализация подсистемы для работы виртуальной памяти);
	\item \texttt{PE\_init\_platfrom} -- на этот раз вызывается с учетом того, что виртуальная память уже инициализирована, \textbf{получает информацию о видеоустройствах и дереве устройств};
	\item \texttt{PE\_create\_console} -- \textbf{стартует консоль} либо в графическом режиме, либо в текстовом;
	\item \texttt{tsc\_init} -- \textbf{инициализация подсистемы работы с TSC"=регистром} (работа через EFI Runtime Services);
	\item \texttt{power\_management\_init} -- \textbf{инициализирует подсистему управления энергией} (работает через регистрацию сторонних расширений ядра);
	\item \texttt{processor\_bootstrap} -- \textbf{инициализация процессора подсистемы Mach"=части};
	\item \texttt{thread\_bootstrap} -- \textbf{инициализирует шаблон для потоковых объектов Mach"=части} (заполнение полей по умолчанию);
	\item \texttt{init\_thread} -- вызывает \texttt{machine\_set\_current\_thread} для \textbf{установки текущего активного потока} на CPU;
	\item \texttt{machine\_startup} -- \textbf{старт системы}.
\end{itemize}

В целом, данная фаза загрузки содержит раннюю инициализацию важных подсистем и включение различных режимов отладки ядра.
\stoptext

\subsection{Функция i386\_init\_slave}
\starttext
Точка входа для slave"=CPU -- это \texttt{slave\_pstart}. Данная функция, в свою очередь, вызывает \texttt{pstart\_common}, но оставляет указатель на структуру boot"=аргументов ядра нулевым; \texttt{do\_init\_slave} -- вызывается на slave"=CPU в первый раз или когда он просыпается после гибернации.
\stoptext

\subsection{Функция machine\_startup}
\starttext
Функция разбирает boot"=аргументы для обнаружения отладочных аргументов, чтобы работать в определенном режиме (с отладчиком или без). Потом обрабатываются еще некоторые аргументы, связанные с планированием работы системы, а затем вызывается \texttt{machine\_conf} для установления поля \texttt{MEMORY\_SIZE} в структуре \texttt{machine\_info}. Далее идет вызов \texttt{kernel\_bootstrap} для дальнейшей инициализации ядра без возврата управления.
\stoptext

\subsection{Функция kernel\_bootstrap}
\starttext
Продолжение инициализации ядра. Помимо виртуальной памяти функция \texttt{kernel\_bootstrap} инициализирует следующие части ядра:
\begin{itemize}
	\item IPC;
	\item часы;
	\item потоки;
	\item mach"=процессы.
\end{itemize}

Функция \texttt{kernel\_bootstrap} запускает первый активный поток системы -- \texttt{kernel\_bootstrap\_thread}. Функция выполняет следующую последовательность действий:
\begin{itemize}
	\item \textbf{выводит версию ядра};
	\item \textbf{проводит парсинг некоторых boot"=аргументов} -- проверяет boot"=аргументы ядра ``-l'' (leaks logging), ``trace'', ``serverperfmode'';
	\item \texttt{scale\_setup} -- \textbf{устанавливает максимальные значения} потоков, процессов, объектов VFS и т.д.;
	\item \texttt{vm\_mem\_bootstrap} -- \textbf{инициализация менеджеров памяти, подсистем виртуальной памяти} (\texttt{vm\_pages}, \texttt{zones}, \texttt{vm\_objects}, \texttt{vm\_maps}, \texttt{kmem}, \texttt{pmap}, \texttt{kalloc}, \texttt{vm\_fault});
	\item \texttt{vm\_mem\_init} -- обертка над \texttt{vm\_object\_init}, по сути, \textbf{заглушка};
	\item \texttt{sched\_init} -- \textbf{инициализация подсистемы планировщика}, выбор планировщика, используя boot"=аргумент \texttt{sched} или значение \texttt{kern.sched} из дерева устройств;
	\item \texttt{wait\_queue} -- \textbf{инициализация зон памяти, которые используются для очередей ожидания};
	\item \texttt{ipc\_bootstrap} -- \textbf{инициализация памяти, которая используется для IPC подсистемы};
	\item \texttt{mac\_policy\_init} -- \textbf{инициализация памяти для MAC подсистемы} (Mandatory Access Control);
	\item \texttt{ipc\_init} -- выделения памяти для IPC;
	\item \texttt{PMAP\_ACTIVATE\_KERNEL} -- \textbf{активирует pmap на master"=CPU}, если определен макрос \texttt{PMAP\_ACTIVATE};
	\item \texttt{mapping\_free\_prime} -- \textbf{освобождает pv"=хэши};
	\item \texttt{machine\_init} -- отображает CPU ID и другие \textbf{особенности CPU}, проводит \textbf{инициализацию EFI и SMP}, \textbf{инициализирует FPU} и \textbf{настраивает системное время}, \textbf{инициализирует MTRR и PAT}, \textbf{освобождает нижние страницы памяти} (подробнее данная функция описана в пункте \ref{subsec:function_machine_init});
	\item \texttt{clock\_init} -- \textbf{инициализация всех часов}, определенных в системе;
	\item \texttt{ledger\_init} -- \textbf{инициализация группы блокировок для ledger'ов};
	\item \texttt{task\_init} -- \textbf{инициализация зон памяти для процессов} (tasks), \textbf{создание процесса kernel\_task};
	\item \texttt{thread\_init} -- \textbf{настройка стека ядра}, \textbf{выделение зон памяти для потоков}, вызов \texttt{machine\_thread\_init} для специфичной инициализации зон памяти для потоков;
	\item \texttt{kernel\_bootstrap\_thread} -- \textbf{создание первого потока} -- потока ядра.
\end{itemize}
\stoptext

\subsection{Функция machine\_init}
\label{subsec:function_machine_init}
\starttext
Вызывает \texttt{smp\_init} для инициализации симметричной мультипроцессорности.
Данная функция разделена на две основные стадии:
\begin{itemize}
	\item \textbf{инициализация LAPIC} -- в SMP архитектуре каждый процессор (или ядро) имеет локальный усовершенствованный программируемый контроллер прерываний (LAPIC), который несет ответственность на аппаратном уровне за доставку прерываний в ядро;
	\item \textbf{установление точки входа для slave"=CPU} -- это делается с помощью копии физической памяти через вызов \texttt{install\_real\_mode\_bootstrap}, точка входа устанавливается в \texttt{slave\_pstart}.
\end{itemize}

Последовательность основных действий функции:
\begin{enumerate}
	\item \texttt{debug\_log\_init} –- \textbf{инициализация panic"=логирования}, не используется в данный момент;
	\item \textbf{отображение особенностей CPU} –- определяет и печатает особенности процессора;
	\item \texttt{efi\_init} -- \textbf{инициализация EFI Runtime Services} и выделение памяти под нужды efi"=подсистемы ядра;
	\item \texttt{smp\_init} -- делится на:
	\begin{enumerate}
		\item \texttt{console\_init} –- \textbf{выделяет память под циклический кольцевой буфер консоли и блокировки чтения/записи};
		\item \texttt{i386\_smp\_init} -- проверяет, есть ли \textbf{локальный контроллер APIC};
		\item \textbf{LAPIC"=инициализация} -- \textbf{инициализация и конфигурация LAPIC}, установка NMI и IPI обработчиков прерываний;
		\item \texttt{install\_real\_mode\_bootstrap} -- \textbf{установление точки входа для slave"=CPU};
		\item \texttt{cpu\_thread\_init};
		\item \texttt{ml\_cpus\_*};
	\end{enumerate}
	\item \texttt{init\_fpu} -- \textbf{инициализация математического сопроцесоора (FPU)} установкой битов в регистре CR0, поддержки SSE(SIMD) и XSAVE через регистр CR4;
	\item \texttt{clock\_config} -- \textbf{настройка календарного реального времени};
	\item \textbf{MTRR настройки} -- \textbf{поддержка Memory Type Range Registers} -- регистров, которые предоставляют механизм, связывающий типы памяти с физическими адресными диапазонами системной памяти;
	\item \texttt{pmap\_lowmem\_finalize} -- \textbf{резервирование страниц в нижней памяти}.
\end{enumerate}
\stoptext

\subsection{Функция kernel\_bootstrap\_thread}
\starttext
Это основной поток системы, который продолжает инициализировать системы, база которых была образована на последних стадиях загрузки. Когда в системе включена поддержка потоков, то \texttt{kernel\_bootstrap\_thread} может вызвать \texttt{kernel\_create\_thread}, чтобы породить дополнительные потоки.

И первый поток, который данная функция создает -- это \textbf{idle"=поток}. Данный поток нужен для того, чтобы система ядер или процессоров всегда что"=то выполняла, когда все другие потоки будут заблокированы.

После idle"=потока создается \textbf{поток планировщика}. Планировщик является процессом, который решает, через определенные промежутки времени или после прерывания, какой поток будет выполняться следующим.

После создания нескольких системных потоков для обслуживания других потоков ядро стартует \texttt{mapping\_replenish} поток. Если ядро настроено с \texttt{SERIAL\_KDP}, то происходит вызов \texttt{init\_kdp}, тем самым \textbf{инициализируется отладчик}.

Следующим важным шагом является \textbf{инициализация IOKit}, фреймворка для драйверов устройств. Данная фаза является ключевым моментом, потому что без IOKit ядро не сможет напрямую обращаться к устройствам: в ядре нет кода даже для простого обращения к диску, сети и дисплею.

После инициализации IOKit \textbf{прерывания могут быть включены}. Это делается с помощью вызова функции \texttt{spllo}, которая вызывает \texttt{ml\_enable\_interrupts}. Данная функция выставляет флаги \texttt{IF EFLAG} для включения прерываний.

Следующий модуль для инициализации -- это \textbf{модуль общей область (shared region module)}, которая используется клиентами, например, dyld при загрузке разделяемых библиотек (также известна как commpage). Commpage -- это одна страница, которая отображается из ядра непосредственно в память всех процессов и содержит различные экспортированные данные, а также функции. Эта страница всегда находится по одному и тому же адресу и доступна для всех процессов.

Если ядро собрано с поддержкой MAC (\texttt{CONFIG\_MACF}), то следующим шагом является вызов \texttt{mac\_policy\_initmach}, что позволяет запуститься как можно раньше \textbf{модулю мандатной политики}.

После того, как MAC включена, \textbf{подсистему BSD можно инициализировать}. Это делается с помощью большой функции \texttt{bsd\_init}. Данная функция в конечном итоге порождает процесс \texttt{init}, который выполняет \textit{/sbin/launchd}, являющуюся прародителем всех процессов пользовательского режима.

После инициализации подсистемы BSD, если ядро было загружено с boot"=аргументом ``serial'', то \textbf{serial"=консоль включается} порождением специального потока для прослушивания консоли. К этому времени процессы пользовательского режима, порожденные после инициализации подсистемы BSD, могут получить доступ к консоли, открыв свой терминал (tty).

В SMP системах на каждом CPU еще \textbf{инициализируется локальная очередь страниц}. И, наконец, происходит \textbf{вызов vm\_pageout}.
\stoptext

\subsection{Функция bsdinit\_task}
\starttext
В конце исполнения \texttt{bsd\_init} делает вызов \texttt{bsd\_utaskbootstrap}. Эта функция косвенно отвечает за старт процесса с PID=1, который является первым процессом, который появится в пользовательском режиме. Для этого он сначала делает вызов \texttt{cloneproc}, который создает новую задачу Mach"=уровня.

Чтобы на самом деле произвести новое ответвление, \texttt{utaskbootstrap} \textbf{генерирует асинхронный системный trap (AST)}, вызвав \texttt{act\_set\_astbsd} в новом созданном потоке. Когда AST обрабатывается, обработчик Mach AST отрабатывает этот частный случай, вызвав \texttt{bsd\_ast}, которая в свою очередь вызывает \texttt{bsdinit\_task}.

Функция \texttt{bsdinit\_task} устанавливает начальное имя процесса как ``init'' в стиле истинной UNIX"=системы. Здесь просто идет копирование в поле \texttt{comm} структуры \texttt{proc\_t}.

Далее идет вызов \texttt{ux\_handler\_init}. Данная функция \textbf{создает отдельный поток ядра}, \texttt{ux\_handler}, который отвечает за \textbf{обработку исключений UNIX} -- т.е. получения сообщений на глобальный \texttt{ux\_exception\_port}. Происходит регистрация порта для исключений, что означает, что все исключения UNIX будут обрабатываться в данном потоке.

Наконец, вызывается \texttt{load\_init\_program}. Функция \texttt{load\_init\_program} отвечает за \textbf{запуск процесса с PID=1}, известного как \textit{launchd}. Для этого он сначала вручную устанавливает массив \texttt{argv[]} в пользовательской памяти. Элемент \texttt{argv[0]} устанавливается в \texttt{init\_program\_name[128] = ``/sbin/launchd''}. При необходимости, если ядро было запущено с аргументом ``-s'' (\texttt{boothowto} тогда равно \texttt{RB\_SINGLE}), то данный флаг также передается \textit{launchd}. После установления массива \texttt{argv[]}, \textit{launchd} запускается с помощью вызова \texttt{execve}.
\stoptext
