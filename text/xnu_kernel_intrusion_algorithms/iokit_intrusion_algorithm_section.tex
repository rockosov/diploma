\section{Внедрение через подсистему IOKit}
\starttext
Алгоритм внедрения в ядро Mac OS X через подсистему IOKit основывается на загрузке расширения ядра KEXT с помощью IOKit"=драйвера, который содержит в своих свойствах словарь IOKitPersonalities, позволяющий загружать IOKit"=драйвер при каждом старте системы. В свойствах такого загрузчика отмечается, что он является драйвером для какого-нибудь типа устройства (например, IOResources), при появлении такого устройства в системе, драйвер будет запущен \cite[стр.43]{iokit_fundamentals}. Исходный код данного алгоритма приведен в приложении \ref{app:iokit_intrusion_algorithm_source_code}.

Основные шаги внедрения включают в себя:
\begin{itemize}
	\item удаление старого заранее слинкованного кэша ядра и расширений ядра (prelinked kernelcache);
	\item распаковка IOKit"=загрузчика и главного расширения ядра в \textit{/System/Library/Extensions};
	\item обновление заранее слинкованного кэша ядра и расширений ядра командой \texttt{kextcache -system-caches}.
\end{itemize}

Данный тип внедрения имеет следующие особенности:
\begin{itemize}
	\item загрузка главного расширения ядра из IOKit"=загрузчика производится с помощью функции \texttt{OSKextLoadKextWithIdentifier()} и скрытие его в системе;
	\item в главном расширении ядра реализована периодическая выгрузка IOKit"=загрузчика для повышения скрытности с помощью функций \texttt{canUnloadKextWithIdentifier()} и \texttt{removeKextWithIdentifier()};
	\item для исключения повторной загрузки главного драйвера реализован механизм отметки в памяти ядра сигнала о его загрузке, на рисунке \ref{fig:iokit_intrusion_mark_placeholder} изображен пример такого места в памяти ядра.
\end{itemize}

Алгоритм загрузки главного драйвера с помощью загрузчика IOKit представлен на рисунке \ref{fig:iokit_intrusion_algorithm}.

\begin{figure}[H]
	\input{algorithms/iokit_intrusion_algorithm}
	\caption{Алгоритм загрузки главного драйвера с помощью загрузчика уровня IOKit}
	\label{fig:iokit_intrusion_algorithm}
\end{figure}

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=1]{iokit_intrusion_mark_placeholder.png}}
	\caption{Место в памяти ядра для отметки о загрузке главного драйвера}
	\label{fig:iokit_intrusion_mark_placeholder}
\end{figure}

На рисунке \ref{fig:iokit_intrusion_info_plist_example} представлен пример файла настройки Info.plist для загрузчика уровня IOKit. С помощью данного файла IOKit"=подсистема ядра загружает драйвер при появлении в системе виртуального устройства AppleKeyStore, которое используется для обработки нажатых клавиш. Данное виртуальное устройство выбрано из-за того, что присутствие его в системе ограничено одним экземпляром. Это с большой вероятностью дает нам утверждать, что IOKit"=загрузчик будет загружен системой лишь единожды.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=0.7]{iokit_intrusion_info_plist_example.png}}
	\caption{Пример файла настройки для загрузчика уровня IOKit}
	\label{fig:iokit_intrusion_info_plist_example}
\end{figure}

Так как загруженный целевой драйвер отображается в списке загруженных расширений ядра (это можно проверить с помощью команды \texttt{kextstat}), то его нужно обязательно скрыть. Скрытие расширений ядра в старых руткитах происходило с помощью удаления драйвера из списка \texttt{kmod\_info} -- списка структур с информацией о загруженных драйверах. В новых версиях ядра Mac OS X данный список не поддерживается основным кодом ядра. Загруженные драйвера коллекционируются в массиве \texttt{OSArray sLoadedKexts}. Соответственно, для скрытия драйвера нужно удалить его из данного массива. Проблемы, с которыми можно столкнуться при решении данной задачи:
\begin{itemize}
	\item массив \texttt{sLoadedKexts} не экспортируется ядром;
	\item недостаточно просто уменьшить количество элементов в массиве, потому что через некоторое время в систему может быть загружен другой драйвер.
\end{itemize}

Первую проблему можно решить путем поиска адреса массива в памяти ядра. На рисунке \ref{fig:disassm_lookupkextwithloadtag} изображен дизассемблированный код метода \texttt{lookupKextWithLoadTag} класса \texttt{OSKext}, который помогает найти адреса массива \texttt{sLoadedKexts}, так как адрес функции IORecursiveLockLock экспортируется, а обращение к массиву расположено сразу за вызовом данной функции. Алгоритм поиска адреса массива \texttt{sLoadedKexts} представлен на рисунке \ref{fig:find_sloadedkexts_algorithm}.

Вторая проблема решается в два действия: уменьшением счетчика ссылок у объекта, представляющего драйвера в массиве, методом \texttt{retain()} и удалением объекта из массива.

В итоге полное скрытие главного драйвера в системе происходит следующим образом:
\begin{itemize}
	\item поиск адреса массива \texttt{OSArray sLoadedKexts};
	\item поиск нужного объекта \texttt{OSKext} по идентификатору в массиве;
	\item уменьшение счетчика ссылок у объекта методом \texttt{retain()};
	\item удаление объекта из массива \texttt{OSArray sLoadedKexts}.
\end{itemize}

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=0.5]{disassm_lookupkextwithloadtag.png}}
	\caption{Дизассемблированный код метода lookupKextWithLoadTag класса OSKext}
	\label{fig:disassm_lookupkextwithloadtag}
\end{figure}

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[height=24cm]{find_sloadedkexts_algorithm.eps}}
	\caption{Алгоритм поиска адреса массива sLoadedKexts}
	\label{fig:find_sloadedkexts_algorithm}
\end{figure}
\stoptextwithfigure
