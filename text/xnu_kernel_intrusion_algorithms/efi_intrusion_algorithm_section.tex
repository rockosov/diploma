\section{Внедрение через интерфейс EFI. Bootkit}
\starttext
Bootkit (буткит) -- это программа, которая осуществляет модификацию загрузочного процесса операционной системы, внедряя свою функциональность на ранние стадии загрузки ОС. Первые буткиты были написаны для модификации главной загрузочной записи MBR и модификации кода загрузки тома (VBR), в следствии чего при загрузке управление передавалось коду буткита перед операционной системой, что позволяло исправлять код ядра ОС, ставить перехваты и осуществлять другую функциональность, которая зависела от целей буткита.

Смысл буткита для интерфейса EFI остается таким же, как и для MBR, VBR. Но технология внедрения, управления ядром операционной системы, архитектура меняются. В подразделе \ref{sec:efi_interface_overview} описана архитектура EFI, и перечислены наиболее явные отличия реализации EFI в компьютерах Apple от стандарта UEFI.
\stoptext

\subsection{Общая архитектура}
\starttext
Стандартный процесс загрузки ядра операционной системы Mac OS X от EFI до его автономной работы представлен на рисунке \ref{fig:efi_xnu_boot_process_clean} и разделен на следующие части:
\begin{itemize}
	\item \textbf{Power ON} -- включение компьютера, начальные стадии инициализации платформы EFI (SEC и PEI из подраздела \ref{sec:efi_interface_overview});
	\item \textbf{boot.efi} -- выполнение загрузчика ядра XNU в среде EFI (стадии DXE и BDS из подраздела \ref{sec:efi_interface_overview});
	\item \textbf{XNU kernel bootstrap} -- ранняя стадия загрузки ядра XNU, подробно описанная в подразделе \ref{sec:xnu_boot_process};
	\item \textbf{XNU kernel is working} -- работа ядра в многозадачном защищенном режиме.
\end{itemize}

В дальнейшем рассмотрении алгоритма внедрения через интерфейс EFI имеется в виду, что код внедрения представляет собой исполняемый файл формата PE32+, содержит в себе все компоненты и главный драйвер для запуска, а также склеивается с оригинальным boot.efi и помещается на его место в \textit{/System/Library/CoreServices/boot.efi}. Данный вариант позволяет производить автостарт кода внедрения вместе с запуском системы. Исходный код алгоритма представлен в приложении \ref{app:efi_intrusion_algorithm_source_code}.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=0.5]{efi_xnu_boot_process_clean.png}}
	\caption{Стандартный процесс загрузки ядра Mac OS X}
	\label{fig:efi_xnu_boot_process_clean}
\end{figure}

На рисунке \ref{fig:efi_xnu_boot_process_with_bootkit} представлен процесс загрузки ядра ОС уже вместе с буткитом (стрелками указана последовательность выполнения кода по архитектурным частям).

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=0.5]{efi_xnu_boot_process_with_bootkit.png}}
	\caption{Загрузка ядра Mac OS X вместе с буткитом}
	\label{fig:efi_xnu_boot_process_with_bootkit}
\end{figure}

Из рисунка \ref{fig:efi_xnu_boot_process_with_bootkit} можно выделить следующие структурные части буткита:
\begin{enumerate}
	\item \textbf{EFI составляющая буткита} -- выполняется в фазе DXE и BDS EFI (подраздел \ref{sec:efi_interface_overview}) и включает в себя следующие компоненты:
	\begin{enumerate}
		\item \textbf{EFI-loader} –- загрузчик Apple boot.efi, отвечает за загрузку оригинального boot.efi в память, установку перехватов, подготовку среды выполнения для компонента EFI-infector;
		\item \textbf{EFI-infector} –- заражение ядра XNU, компонент, отвечающий за установку перехватов в ядре XNU, подготавливающий дальнейшие компоненты (preloader и loader) буткита к исполнению;
	\end{enumerate}
	\item \textbf{XNU составляющая буткита} -- выполняется во время загрузки ядра XNU (подраздел \ref{sec:xnu_boot_process}) и в дальнейшем параллельно ему и содержит компоненты:
	\begin{enumerate}
		\item \textbf{preloader} –- компонент, задача которого состоит в обеспечении правильной параллельной передачи управления от ядра XNU к компоненту loader;
		\item \textbf{loader} –- загрузчик расширений ядра, компонент, который занимается скрытной загрузкой исполняемого кода драйвера в память ядра и его исполнением.
	\end{enumerate}
\end{enumerate}
\stoptext

\subsection{Компонент EFI-loader}
\starttext
Первой стадией работы компонента EFI-loader является \textbf{распаковка оригинального boot.efi на EFI FAT раздел} -- производит распаковку оригинального boot.efi на EFI FAT раздел, который доступен на запись, для последующей загрузки boot.efi в память (рисунки \ref{fig:efi_scan_available_volumes_algorithm}, \ref{fig:boot_efi_load_process}, \ref{fig:load_boot_efi_procedures_algorithm}).

\begin{figure}[H]
	\input{algorithms/efi_scan_available_volumes_algorithm}
	\caption{Алгоритм сканирования разделов EFI}
	\label{fig:efi_scan_available_volumes_algorithm}
\end{figure}

\begin{figure}[pH]
	\input{algorithms/load_boot_efi_algorithm}
	\caption{Алгоритм загрузки boot.efi в память компонентом EFI-loader}
	\label{fig:boot_efi_load_process}
\end{figure}

\begin{figure}[H]
	\input{algorithms/load_boot_efi_procedures_algorithm}
	\caption{Процедуры, использующиеся для загрузки boot.efi в память}
	\label{fig:load_boot_efi_procedures_algorithm}
\end{figure}

Далее выполняются следующие стадии компонента:
\begin{itemize}
	\item \textbf{загрузка boot.efi в память} -- осуществляется с помощью Boot-сервиса \texttt{LoadImage};
	\item \textbf{установка перехвата управления boot.efi} -- производится путем исправления адреса функции \texttt{ExitBootServices} среды EFI на собственный обработчик (есть и другие варианты) (подраздел \ref{sec:efi_interface_overview});
	\item \textbf{передача управления boot.efi} -- осуществляется с помощью Boot-сервиса \texttt{StartImage}.
\end{itemize}
\stoptext

\subsection{Компонент EFI-infector}
\starttext
Данный компонент вызывается в контексте перехвата управления от boot.efi с уверенностью в том, кто ядро XNU загружено в память, релоцировно, но ему еще не передано управление. Включает в себя следующие шаги:
\begin{itemize}
	\item поиск базового адреса загрузки ядра XNU;
	\item загрузка кода компонента loader в физическую память, релокация компонента loader, подготовка его к загрузке;
	\item загрузка компонента preloader в ядро, установка перехвата в ядре, поиск адресов нужных символов для preloder;
	\item отдача управления boot.efi.
\end{itemize}

Проблемы реализации алгоритма компонента EFI-infector:
\begin{itemize}
	\item рандомизация адресного пространства KASLR ядра XNU;
	\item резервирование физической оперативной памяти для компонента loader и компонента preloader;
	\item выбор удобного места перехвата в ядре.
\end{itemize}

KASLR ядра XNU -- размещение ядра по случайному адресу. Базовый адрес ядра нужен для разрешения символов и исправления кода ядра в памяти. Для поиска адреса ядра в условиях KASLR предлагается использовать следующие методы:
\begin{itemize}
	\item поиск в адресном пространстве boot.efi boot"=аргументов, в которых содержится значение параметра \texttt{slide} ядра –- смещения относительно базового адреса \texttt{0xffffff8000200000};
	\item поиск в адресном пространстве boot.efi значения глобальной переменной \texttt{slide} ядра;
	\item поиск базового адреса ядра по карте памяти через Boot-сервис \texttt{GetMemoryMap}.
\end{itemize}

Решено использовать последний метод поиска базового адреса ядра, потому что данный способ наиболее архитектурно независим и срабатывает в 100 процентах случаев.

Для решения второй проблемы был исследован код загрузчика boot.efi с помощью техник reverse engineering, а также исследована последовательность загрузки ядра XNU (подраздел \ref{sec:xnu_boot_process}). На рисунке \ref{fig:xnu_virtual_memory_management} представлено управление виртуальной памятью в XNU. В коде загрузчика есть функция, которая отвечает за выделение физической оперативной памяти для ядра и Runtime"=сервисов, прототип которой изображен на рисунке \ref{fig:alloc_combined_kernel_efi_mem}.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[scale=1]{xnu_virtual_memory_management.png}}
	\caption{Управление виртуальной памятью в XNU}
	\label{fig:xnu_virtual_memory_management}
\end{figure}

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=\textwidth]{alloc_combined_kernel_efi_mem.png}}
	\caption{Прототип функции выделения памяти для ядра из загрузчика}
	\label{fig:alloc_combined_kernel_efi_mem}
\end{figure}

Отсюда следует, что память, которую можно выделить данной функцией, будет зарезервирована для ядра и никогда не будет использована для трансляции виртуальной памяти. Кроме того, плюс такой памяти в том, что ее адреса полностью совпадают с виртуальными адресами в нижней 8-байтной половине (верхние 8 байт равны \texttt{0xffffff80}). На рисунке \ref{fig:efi_infector_alloc_mem} изображено разбиение памяти после выделения памяти компонентом EFI-infector под loader и preloader.

\begin{figure}[H]
	\centering
	\fbox{\includegraphics[width=\textwidth]{efi_infector_alloc_mem.eps}}
	\caption{Разбиение памяти после выделения места под loader}
	\label{fig:efi_infector_alloc_mem}
\end{figure}

В конце исполнения компонента EFI-infector происходит перехват функции \texttt{load\_init\_program}, что позволяет компоненту preloader получить управление в тот момент исполнения ядра, когда в создание потоков уже разрешено (подраздел \ref{sec:xnu_boot_process}).

В итоге, алгоритм компонента EFI-infector содержит в себе следующую последовательность действий:
\begin{enumerate}
	\item получение полного размера компонента loader;
	\item \label{itm:alloc_mem_for_loader_and_preloader} выделение физической памяти для компонента loader и компонента preloader, как показано на рисунке \ref{fig:efi_infector_alloc_mem};
	\item перевод адреса физической памяти в адрес виртуальной памяти сложением с \texttt{0xffffff8000000000};
	\item разрешение символов ядра, необходимых компоненту loader;
	\item релокация секций компонента loader;
	\item внутренняя и внешняя релокация, описанная в пункте \ref{subsec:macho_relocation};
	\item релокация сегментов компонента loader;
	\item загрузка компонента loader в память, выделенную на шаге \ref{itm:alloc_mem_for_loader_and_preloader};
	\item загрузка компонента preloader в память, выделенную на шаге \ref{itm:alloc_mem_for_loader_and_preloader};
	\item передача управления boot.efi.
\end{enumerate}

Для разрешения символов ядра и релокации компонента loader используется алгоритм поиска адреса символа в Mach-O файле, показанный на рисунке \ref{fig:macho_find_symbol64}.

\begin{figure}[H]
	\input{algorithms/find_kernel_symbol_algorithm}
	\caption{Алгоритм поиска адреса символа в Mach-O файле}
	\label{fig:macho_find_symbol64}
\end{figure}
\stoptextwithfigure

\subsection{Компонент preloader}
\starttext
Компонент preloader написан на NASM"=ассемблере в виде шеллкода, помещается и исполняется в памяти, выделенной под preloader и loader компонентом EFI-infector.

Выполняет следующие действия:
\begin{itemize}
	\item печатает сообщение в кольцевой буфер ядра в отладочном режиме;
	\item запускает поток ядра с главной функцией компонента loader –- \texttt{loaderThreadContinuation} (отсюда начинается параллельное исполнение loader);
	\item передает управление ядру XNU.
\end{itemize}
\stoptext

\subsection{Компонент loader}
\starttext
Представляет собой Mach-O файл, который принимает на вход драйвер и загружает его в память аналогично системному загрузчику KXLD.

KXLD"=подсистема отличается следующими особенностями \cite{kernel_programming_guide}:
\begin{itemize}
	\item подходит для любого типа загрузки Mach-O файлов, то есть может быть использована и для загрузки пользовательских приложений;
	\item поддерживает все архитектуры файлов, заявленные для Mac OS X;
	\item использует собственную объектную модель для решения задач загрузки файла.
\end{itemize}

Процесс загрузки расширения ядра включает в себя следующие этапы:
\begin{enumerate}
	\item разрешение зависимостей на уровне класса \texttt{OSKext};
	\item загрузка в память всех зависимостей;
	\item \label{itm:alloc_mem_for_kext} выделение памяти для размещения кода в пространстве ядра;
	\item коллекционирование символов зависимостей;
	\item коллекционирование символов текущего расширения ядра;
	\item релокация сегментов, секций, таблицы символов;
	\item разрешение импортированных символов;
	\item создание и правка виртуальных таблиц для классов, применяемых в расширении ядра; 
	\item проверка правильности разрешения всех символов;
	\item внешняя и внутренняя релокация;
	\item размещение Mach-O файла в виртуальной памяти, выделенной на шаге \ref{itm:alloc_mem_for_kext};
	\item установка VM битов защиты для сегментов и секций индивидуально;
	\item добавление расширения ядра в массив загруженных расширений.
\end{enumerate}

Загрузка произвольного кода в пространство ядра происходит по следующему алгоритму:
\begin{enumerate}
	\item \label{itm:alloc_mem_for_kext2} выделение памяти для размещения кода в пространстве ядра;
	\item коллекционирование символов зависимостей;
	\item разрешение импортированных символов из ядра, обходя интерфейсы (например, \texttt{com.apple.kpi.bsd}) по алгоритму из рисунка \ref{fig:macho_find_symbol64};
	\item релокация внутренних символов, которые находятся в секциях;
	\item внешняя и внутреняя релокация;
	\item релокация сегментов, секций, таблицы символов (пункт \ref{subsec:macho_relocation});
	\item размещение Mach-O файла в виртуальной памяти, выделенной на шаге \ref{itm:alloc_mem_for_kext2};
	\item установка VM битов защиты для сегментов и секций индивидуально.
\end{enumerate}

После того, как код загружен в память и правильно слинкован, можно начинать его выполнение:
\begin{itemize}
	\item установка защиты виртуальной памяти драйвера;
	\item инициализация CPP-runtime для драйвера (на случай, если часть драйвера написано на C++);
	\item вызов конструктора драйвера.
\end{itemize}

Данный алгоритм позволяет загружать любой код уровня ядра в пространство ядра скрытно и параллельно любой системе ядра.
\stoptext
